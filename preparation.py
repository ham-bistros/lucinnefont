#!usr/bin/python

import os

charlist = [ 'A', 'Á', 'À', 'Â', 'Ä', 'B', 'C', 'D', 'E', 'É', 'È', 'Ê', 'Ë', 'F', 'G', 'H', 'I', 'Í', 'Ì', 'Ï', 'Î', 'J', 'K', 'L', 'M', 'N', 'O', 'Ó', 'Ò', 'Ô', 'Ö', 'P', 'Q', 'R', 'S', 'T', 'U', 'Ò', 'Ó', 'Ö', 'Ô', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ',', '.', '-', ';', ':', '?', '!', '\'', '\"', '@', '+', '…', '&', '·', '/']

charlist_ord = []

for char in charlist:
    charlist_ord.append(ord(char))

charlist_ord = set(charlist_ord)

for ch in charlist_ord:
    folder = 'glyphs/%s' % ch

    if not os.path.isdir(folder):
        print('creating folder %s…' % folder)
        os.mkdir(folder)
# print('Tous les nouveaux dossiers ont été crées 😎')

for file in os.listdir('tmp'):
    letter = file.split('-')[0]
    try:
        print(ord(letter))
    except TypeError:
        print(letter)
