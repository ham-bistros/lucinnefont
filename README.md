# lucinnefont

A font from hand-drawn lettering of [Lucinne Salva](https://www.instagram.com/lucinnesalva/). Attempts to make it feel somewhat lively by adding pseudo-randomness with open-type substitutions.

Made 100% with FLOSS yay \o/

[osp's tool glyphtracer](https://github.com/osp/osp.tools.fons)

## Inspirations
+ [TT2020 "realistic" typewriter font](https://copypaste.wtf/TT2020/docs/download.html), by Frederick Brennan, who described his process [here](https://copypaste.wtf/TT2020/docs/moreinfo2.html)
+

## License
[OFL](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl) for now I guess ?
